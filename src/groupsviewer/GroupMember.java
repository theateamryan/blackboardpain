package groupsviewer;

public class GroupMember 
{
	String firstname = "";
	String lastname = "";
	String userid = "";
	String studentNumber = "";
	String group = "";
	
	public GroupMember(String firstname, String lastname, String userid, String studentNumber, String group)
	{
		this.firstname = firstname;
		this.lastname = lastname;
		this.userid = userid;
		this.studentNumber = studentNumber;
		if((group == null) || (group.equals("")))
		{
			this.group = "< NOT ASSIGNED >";
		}
		else
		{
			this.group = group;
		}
	}
	
	public String getFirstName()
	{
		return firstname;
	}
	
	public String getLastName()
	{
		return lastname;
	}
	
	public String getName()
	{
		return firstname + " " + lastname;
	}
	
	public String getUserId()
	{
		return userid;
	}
	
	public String getStudentNumber()
	{
		return studentNumber;
	}
	
	public String getGroup()
	{
		return group;
	}
}