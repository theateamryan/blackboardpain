package groupsviewer;


import blackboard.base.BaseComparator;
import blackboard.base.GenericFieldComparator;
import blackboard.data.course.Course;
import blackboard.data.course.CourseMembership;
import blackboard.data.user.User;
import blackboard.persist.user.UserDbLoader;
import blackboard.platform.context.Context;
import blackboard.platform.context.ContextManagerFactory;
import blackboard.platform.session.BbSession;
import blackboard.platform.session.BbSessionManagerService;
import blackboard.platform.session.BbSessionManagerServiceFactory;
//
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.ServletContext;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Controller
public class GroupController implements ServletContextAware {

    private ServletContext servletContext;

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    @RequestMapping(value = "/course_tool", method = RequestMethod.GET)
    public Object controlPanelTool(ModelAndView model) throws Exception {
        Context ctx = ContextManagerFactory.getInstance().getContext();
        
     //   BbSessionManagerService sessionService = BbSessionManagerServiceFactory.getInstance();
     //	  BbSession bbSession = sessionService.getSession(request);
     //	  CourseMembership courseMembership = ctx.getCourseMembership();
         
        	Course course = ctx.getCourse();
        	String courseId = course.getCourseId();
     //     String courseReqId = "TestCourseReqId"; // String courseReqId = request.getParameter("course_id");
     //     String downloadUrl = "download?course_id=" + courseReqId;

        GenericFieldComparator<GroupMember> firstNameComparator = new GenericFieldComparator<GroupMember>( BaseComparator.ASCENDING, "getFirstName()", GroupMember.class);
        
        
    //  GenericFieldComparator<GroupMember> lastNameComparator = new GenericFieldComparator<GroupMember>( BaseComparator.ASCENDING, "getLastName", GroupMember.class );
    //	GenericFieldComparator<GroupMember> userIdComparator = new GenericFieldComparator<GroupMember>( BaseComparator.ASCENDING, "getUserId", GroupMember.class );
	//	GenericFieldComparator<GroupMember> studentNumberComparator = new GenericFieldComparator<GroupMember>( BaseComparator.ASCENDING, "getStudentNumber", GroupMember.class );
	//	GenericFieldComparator<GroupMember> groupComparator = new GenericFieldComparator<GroupMember>( BaseComparator.ASCENDING, "getGroup", GroupMember.class );
      
		String test = "TestString";
		model.addObject("test", test);		//Just making it feeds through
        model.addObject("firstNameComparator", firstNameComparator);
        
        model.setViewName("group"); // name of the JSP file to use
        // Blackboard doesn't always pass the bundle to the JSP when using spring, so we'll do it manually
        LocalizationContext localizationContext = (LocalizationContext) servletContext.getAttribute("javax.servlet.jsp.jstl.fmt.localizationContext.application");
        model.addObject("uweBundle", localizationContext);
        return model;
    }

}
