package groupsviewer;import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;

import blackboard.blti.message.Context;
import blackboard.db.BbDatabase;
import blackboard.db.ConnectionNotAvailableException;
import blackboard.platform.context.ContextManagerFactory;
import blackboard.platform.session.BbSession;
import blackboard.platform.session.BbSessionManagerService;
import blackboard.platform.session.BbSessionManagerServiceFactory;
import blackboard.data.course.CourseMembership;
import blackboard.data.user.User;
import blackboard.persist.user.UserDbLoader;
import blackboard.platform.context.ContextManagerFactory;

@Controller
public class GroupMemberList 
{
	private static final String GET_ALL =
		"SELECT USERS.FIRSTNAME, USERS.LASTNAME, USERS.USER_ID, USERS.STUDENT_ID, GROUPS.GROUP_NAME, COURSE_MAIN.COURSE_ID"
		+	" FROM COURSE_USERS"
		+	" left join GROUP_USERS ON GROUP_USERS.COURSE_USERS_PK1 = COURSE_USERS.PK1"
		+	" left join GROUPS ON GROUPS.PK1 = GROUP_USERS.GROUPS_PK1"
		+	" inner join USERS ON USERS.PK1 = COURSE_USERS.USERS_PK1"
		+	" inner join COURSE_MAIN ON COURSE_MAIN.PK1 = COURSE_USERS.CRSMAIN_PK1"
		+ " WHERE (COURSE_MAIN.COURSE_ID = ?) AND (COURSE_USERS.ROW_STATUS = 0)";
	
	private static final String GET_TYPE =
		"SELECT USERS.FIRSTNAME, USERS.LASTNAME, USERS.USER_ID, USERS.STUDENT_ID, GROUPS.GROUP_NAME, COURSE_MAIN.COURSE_ID"
		+	" FROM COURSE_USERS"
		+	" left join GROUP_USERS ON GROUP_USERS.COURSE_USERS_PK1 = COURSE_USERS.PK1"
		+	" left join GROUPS ON GROUPS.PK1 = GROUP_USERS.GROUPS_PK1"
		+	" inner join USERS ON USERS.PK1 = COURSE_USERS.USERS_PK1"
		+	" inner join COURSE_MAIN ON COURSE_MAIN.PK1 = COURSE_USERS.CRSMAIN_PK1"
		+ " WHERE (COURSE_MAIN.COURSE_ID = ?) AND (COURSE_USERS.ROW_STATUS = 0) AND (COURSE_USERS.ROLE = ?)";
		
	public static List<GroupMember> getMembersList(String courseId, String userType)
	{
	
		BbDatabase db = BbDatabase.getDefaultInstance();
		Connection dbConnection = null;
		List<GroupMember> groupMembersList = null;
		// blackboard.platform.context.Context ctx = ContextManagerFactory.getInstance().getContext();
		
		
		if(userType == null)
		{
			userType = "all";
		}
		
		try 
		{
			dbConnection = db.getConnectionManager().getConnection();
			PreparedStatement statement;
			
			if(userType.equals("student"))
			{
				statement = dbConnection.prepareStatement(GET_TYPE);
				statement.setString(2, "S");
			}
			else if (userType.equals("instructor"))
			{
				statement = dbConnection.prepareStatement(GET_TYPE);
				statement.setString(2, "P");
			}
			else
			{
				statement = dbConnection.prepareStatement(GET_ALL);
			}
			
			statement.setString(1, courseId);

			ResultSet results = statement.executeQuery();
			
			groupMembersList = new ArrayList<groupsviewer.GroupMember>();
			
			//populate list with the results from the database query
			while(results.next())
			{
				groupsviewer.GroupMember member = new groupsviewer.GroupMember(results.getString("FIRSTNAME"),
				results.getString("LASTNAME"),
				results.getString("USER_ID"),
				results.getString("STUDENT_ID"),
				results.getString("GROUP_NAME"));
				
				groupMembersList.add(member);
			}
		}
		catch (ConnectionNotAvailableException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			//finished with the connection now so release it!
			db.getConnectionManager().releaseConnection(dbConnection);
		}
		
		return groupMembersList;
	}
	
	
}