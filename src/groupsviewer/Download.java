package groupsviewer;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import blackboard.platform.context.Context;
import blackboard.platform.context.ContextManagerFactory;

/**
 * Servlet for downloading the list of group member as a CSV file.
 * 
 * @author nj2-fielding
 *
 */
public class Download extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException 
	{
		resp.reset();
		
		Context ctx = ContextManagerFactory.getInstance().getContext();
		String courseId = ctx.getCourse().getCourseId();
		String userType = req.getParameter("userType");
		List<GroupMember> groupMembers = GroupMemberList.getMembersList(courseId, userType);
		String groupListCsv = groupListToCsv(groupMembers);
					
		ServletOutputStream out = resp.getOutputStream();
		resp.addHeader("Content-Disposition","attachment; filename=\"" + courseId + "-groupListing.csv\"");
		resp.setContentType("text/plain");
		out.print(groupListCsv);
		out.flush();
		out.close();
	}
	
	/**
	 * Converts the GroupMember List into a comma separated string.
	 * 
	 * @param groupMembers
	 * @return
	 */
	private String groupListToCsv(List<GroupMember> groupMembers)
	{
		StringBuilder csvBuilder = new StringBuilder();
		//first apply the headers
		csvBuilder.append("Last Name,First Name,UserID,Student Number,Group\r\n");
		//now add each entry
		for(GroupMember groupMember : groupMembers)
		{
			csvBuilder.append(groupMember.getLastName() + ","
					+ groupMember.getFirstName() + ","
					+ groupMember.getUserId() + ","
					+ groupMember.getStudentNumber() + ","
					+ groupMember.getGroup() + "\r\n");
		}
		
		return csvBuilder.toString();
	}
}