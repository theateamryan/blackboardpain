<%@ taglib uri="/bbNG" prefix="bbNG"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>




<bbNG:learningSystemPage ctxId="ctx" title="GroupViewer">
        
        ${test}
        
        <bbNG:inventoryList className="blackboard.data.user.User"
                           collection="${courseUsers}"
                            objectVar="courseUser"
                            emptyMsg="${firstNameComparator}" >
                            
            <bbNG:listElement label="FirstName" name="FirstName" isRowHeader="true" comparator="${firstNameComparator}">
                ${fn:escapeXml(courseUser.userName)}
            </bbNG:listElement>
            <bbNG:listElement label="Full Name" name="fullName">
                ${fn:escapeXml(courseUser.givenName)} ${fn:escapeXml(courseUser.familyName)}
            </bbNG:listElement> 
        </bbNG:inventoryList> 
        
       

</bbNG:learningSystemPage>











   <%--      <bbNG:inventoryList className="blackboard.data.user.User"
                            collection="${courseUsers}"
                            objectVar="courseUser"
                            emptyMsg="There are no users in this course"
        >
            <bbNG:listElement label="UserName" name="userName" isRowHeader="true" comparator="${userNameComparator}">
                ${fn:escapeXml(courseUser.userName)}
            </bbNG:listElement>
            <bbNG:listElement label="Full Name" name="fullName">
                ${fn:escapeXml(courseUser.givenName)} ${fn:escapeXml(courseUser.familyName)}
            </bbNG:listElement> 
        </bbNG:inventoryList> --%>